# Copyright (C) 2013 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file is generated by device/lge/vu2u/setup-makefiles.sh

PRODUCT_COPY_FILES += \
    vendor/lge/vu2u/proprietary/lib/libacdbloader.so:system/lib/libacdbloader.so \
    vendor/lge/vu2u/proprietary/lib/libuicc.so:system/lib/libuicc.so \
    vendor/lge/vu2u/proprietary/lib/libadsprpc.so:system/lib/libadsprpc.so \
    vendor/lge/vu2u/proprietary/lib/libcneutils.so:system/lib/libcneutils.so \
    vendor/lge/vu2u/proprietary/lib/libaudcal.so:system/lib/libaudcal.so \
    vendor/lge/vu2u/proprietary/lib/libmmjpeg.so:system/lib/libmmjpeg.so \
    vendor/lge/vu2u/proprietary/lib/libmmipl.so:system/lib/libmmipl.so \
    vendor/lge/vu2u/proprietary/lib/libgemini.so:system/lib/libgemini.so \
    vendor/lge/vu2u/proprietary/lib/libmercury.so:system/lib/libmercury.so \
    vendor/lge/vu2u/proprietary/lib/libmmstillomx.so:system/lib/libmmstillomx.so \
    vendor/lge/vu2u/proprietary/lib/libimage-jpeg-dec-omx-comp.so:system/lib/libimage-jpeg-dec-omx-comp.so \
    vendor/lge/vu2u/proprietary/lib/libimage-jpeg-enc-omx-comp.so:system/lib/libimage-jpeg-enc-omx-comp.so \
    vendor/lge/vu2u/proprietary/lib/libimage-omx-common.so:system/lib/libimage-omx-common.so \
    vendor/lge/vu2u/proprietary/lib/libmm-color-convertor.so:system/lib/libmm-color-convertor.so \
    vendor/lge/vu2u/proprietary/lib/libacdbloader.so:system/lib/libacdbloader.so \
    vendor/lge/vu2u/proprietary/lib/libdiag.so:system/lib/libdiag.so \
    vendor/lge/vu2u/proprietary/lib/libril-qc-qmi-1.so:system/lib/libril-qc-qmi-1.so \
    vendor/lge/vu2u/proprietary/lib/libril-qcril-hook-oem.so:system/lib/libril-qcril-hook-oem.so \
    vendor/lge/vu2u/proprietary/lib/libril.so:system/lib/libril.so \
    vendor/lge/vu2u/proprietary/lib/liboemcamera.so:system/lib/liboemcamera.so \
    vendor/lge/vu2u/proprietary/lib/libsensor1.so:system/lib/libsensor1.so \
    vendor/lge/vu2u/proprietary/lib/libidl.so:system/lib/libidl.so \
    vendor/lge/vu2u/proprietary/lib/libqc-opt.so:system/lib/libqc-opt.so \
    vendor/lge/vu2u/proprietary/lib/libproxyhal.so:system/lib/libproxyhal.so \
    vendor/lge/vu2u/proprietary/lib/libOlaLGECameraJNI_4.so:system/lib/libOlaLGECameraJNI_4.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_faceproc.so:system/lib/libmmcamera_faceproc.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_frameproc.so:system/lib/libmmcamera_frameproc.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_hdr_lib.so:system/lib/libmmcamera_hdr_lib.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_image_stab.so:system/lib/libmmcamera_image_stab.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_plugin.so:system/lib/libmmcamera_plugin.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_statsproc31.so:system/lib/libmmcamera_statsproc31.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_wavelet_lib.so:system/lib/libmmcamera_wavelet_lib.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_interface.so:system/lib/libmmcamera_interface.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_interface2.so:system/lib/libmmcamera_interface2.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_3a_legacy.so:system/lib/libmmcamera_3a_legacy.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_tintless_algo.so:system/lib/libmmcamera_tintless_algo.so \
    vendor/lge/vu2u/proprietary/lib/libmmcamera_tintless_wrapper.so:system/lib/libmmcamera_tintless_wrapper.so \
    vendor/lge/vu2u/proprietary/lib/libmorpho_noise_reduction.so:system/lib/libmorpho_noise_reduction.so \
    vendor/lge/vu2u/proprietary/lib/libmorpho_memory_allocator.so:system/lib/libmorpho_memory_allocator.so \
    vendor/lge/vu2u/proprietary/lib/libmorpho_panorama_gp.so:system/lib/libmorpho_panorama_gp.so \
    vendor/lge/vu2u/proprietary/lib/libmorpho_panorama_wa_viewer.so:system/lib/libmorpho_panorama_wa_viewer.so \
    vendor/lge/vu2u/proprietary/lib/libHDR.so:system/lib/libHDR.so \
    vendor/lge/vu2u/proprietary/lib/libmmjpeg_interface.so:system/lib/libmmjpeg_interface.so \
    vendor/lge/vu2u/proprietary/lib/libchromatix_imx111_preview.so:system/lib/libchromatix_imx111_preview.so \
    vendor/lge/vu2u/proprietary/lib/libchromatix_imx111_zsl.so:system/lib/libchromatix_imx111_zsl.so \
    vendor/lge/vu2u/proprietary/lib/libchromatix_imx119_video.so:system/lib/libchromatix_imx119_video.so \
    vendor/lge/vu2u/proprietary/lib/libchromatix_imx119_preview.so:system/lib/libchromatix_imx119_preview.so \
    vendor/lge/vu2u/proprietary/lib/libchromatix_imx119_vt.so:system/lib/libchromatix_imx119_vt.so \
    vendor/lge/vu2u/proprietary/lib/libmmQSM.so:system/lib/libmmQSM.so \
    vendor/lge/vu2u/proprietary/lib/libdsutils.so:system/lib/libdsutils.so \
    vendor/lge/vu2u/proprietary/lib/libqmi.so:system/lib/libqmi.so \
    vendor/lge/vu2u/proprietary/lib/libqmi_csvt_srvc.so:system/lib/libqmi_csvt_srvc.so \
    vendor/lge/vu2u/proprietary/lib/libmmosal.so:system/lib/libmmosal.so \
    vendor/lge/vu2u/proprietary/lib/libmmparser.so:system/lib/libmmparser.so \
    vendor/lge/vu2u/proprietary/lib/libmmstillomx.so:system/lib/libmmstillomx.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libqmi_cci.so:system/vendor/lib/libqmi_cci.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libqmi_csi.so:system/vendor/lib/libqmi_csi.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libqmi_common_so.so:system/vendor/lib/libqmi_common_so.so \
    vendor/lge/vu2u/proprietary/lib/libqmiservices.so:system/lib/libqmiservices.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libqmi_encdec.so:system/vendor/lib/libqmi_encdec.so \
    vendor/lge/vu2u/proprietary/lib/libqmi_client_qmux.so:system/lib/libqmi_client_qmux.so \
    vendor/lge/vu2u/proprietary/lib/libqcci_legacy.so:system/lib/libqcci_legacy.so \
    vendor/lge/vu2u/proprietary/lib/libdsi_netctrl.so:system/lib/libdsi_netctrl.so \
    vendor/lge/vu2u/proprietary/lib/libqdi.so:system/lib/libqdi.so \
    vendor/lge/vu2u/proprietary/lib/libnetmgr.so:system/lib/libnetmgr.so \
    vendor/lge/vu2u/proprietary/lib/libqdp.so:system/lib/libqdp.so \
    vendor/lge/vu2u/proprietary/lib/libacdbloader.so:system/lib/libacdbloader.so \
    vendor/lge/vu2u/proprietary/lib/libaudioalsa.so:system/lib/libaudioalsa.so \
    vendor/lge/vu2u/proprietary/lib/libalsautils.so:system/lib/libalsautils.so \
    vendor/lge/vu2u/proprietary/lib/libCommandSvc.so:system/lib/libCommandSvc.so \
    vendor/lge/vu2u/proprietary/lib/libconfigdb.so:system/lib/libconfigdb.so \
    vendor/lge/vu2u/proprietary/lib/libcsd-client.so:system/lib/libcsd-client.so \
    vendor/lge/vu2u/proprietary/lib/libdrmdiag.so:system/lib/libdrmdiag.so \
    vendor/lge/vu2u/proprietary/lib/libdrmfs.so:system/lib/libdrmfs.so \
    vendor/lge/vu2u/proprietary/lib/libdrmtime.so:system/lib/libdrmtime.so \
    vendor/lge/vu2u/proprietary/lib/libdss.so:system/lib/libdss.so \
    vendor/lge/vu2u/proprietary/lib/libQSEEComAPI.so:system/lib/libQSEEComAPI.so \
    vendor/lge/vu2u/proprietary/lib/libsensor_reg.so:system/lib/libsensor_reg.so \
    vendor/lge/vu2u/proprietary/lib/libsensor_user_cal.so:system/lib/libsensor_user_cal.so \
    vendor/lge/vu2u/proprietary/lib/libxml.so:system/lib/libxml.so \
    vendor/lge/vu2u/proprietary/lib/libmm-abl.so:system/lib/libmm-abl.so \
    vendor/lge/vu2u/proprietary/lib/libmm-abl-oem.so:system/lib/libmm-abl-oem.so \
    vendor/lge/vu2u/proprietary/lib/libWVphoneAPI.so:system/lib/libWVphoneAPI.so \
    vendor/lge/vu2u/proprietary/lib/hw/camera.msm8960.so:system/lib/hw/camera.msm8960.so \
    vendor/lge/vu2u/proprietary/lib/hw/sensors.msm8960.so:system/lib/hw/sensors.msm8960.so \
    vendor/lge/vu2u/proprietary/vendor/lib/egl/eglsubAndroid.so:system/vendor/lib/egl/eglsubAndroid.so \
    vendor/lge/vu2u/proprietary/vendor/lib/egl/libEGL_adreno.so:system/vendor/lib/egl/libEGL_adreno.so \
    vendor/lge/vu2u/proprietary/vendor/lib/egl/libGLESv1_CM_adreno.so:system/vendor/lib/egl/libGLESv1_CM_adreno.so \
    vendor/lge/vu2u/proprietary/vendor/lib/egl/libGLESv2_adreno.so:system/vendor/lib/egl/libGLESv2_adreno.so \
    vendor/lge/vu2u/proprietary/vendor/lib/egl/libq3dtools_adreno.so:system/vendor/lib/egl/libq3dtools_adreno.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libC2D2.so:system/vendor/lib/libC2D2.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libllvm-arm.so:system/vendor/lib/libllvm-arm.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libadreno_utils.so:system/vendor/lib/libadreno_utils.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libOpenVG.so:system/vendor/lib/libOpenVG.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libOpenCL.so:system/vendor/lib/libOpenCL.so \
    vendor/lge/vu2u/proprietary/lib/libloc_api_v02.so:system/lib/libloc_api_v02.so \
    vendor/lge/vu2u/proprietary/lib/libloc_adapter.so:system/lib/libloc_adapter.so \
    vendor/lge/vu2u/proprietary/lib/libloc_eng.so:system/lib/libloc_eng.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libloc_ext.so:system/vendor/lib/libloc_ext.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libsc-a2xx.so:system/vendor/lib/libsc-a2xx.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libgsl.so:system/vendor/lib/libgsl.so \
    vendor/lge/vu2u/proprietary/lib/libami306.so:system/lib/libami306.so \
    vendor/lge/vu2u/proprietary/lib/liblgftmitem.so:system/lib/liblgftmitem.so \
    vendor/lge/vu2u/proprietary/lib/hw/nfc_lg.default.so:system/lib/hw/nfc_lg.default.so \
    vendor/lge/vu2u/proprietary/lib/libchromatix_imx111_video_default.so:system/lib/libchromatix_imx111_video_default.so \
    vendor/lge/vu2u/proprietary/vendor/lib/hw/power.qcom.so:system/vendor/lib/hw/power.qcom.so \
    vendor/lge/vu2u/proprietary/vendor/lib/libWVStreamControlAPI_L1.so:system/vendor/lib/libWVStreamControlAPI_L1.so \
    vendor/lge/vu2u/proprietary/bin/ATFWD-daemon:system/bin/ATFWD-daemon \
    vendor/lge/vu2u/proprietary/bin/diag_klog:system/bin/diag_klog \
    vendor/lge/vu2u/proprietary/bin/diag_mdlog:system/bin/diag_mdlog \
    vendor/lge/vu2u/proprietary/bin/ds_fmc_appd:system/bin/ds_fmc_appd \
    vendor/lge/vu2u/proprietary/bin/netmgrd:system/bin/netmgrd \
    vendor/lge/vu2u/proprietary/bin/port-bridge:system/bin/port-bridge \
    vendor/lge/vu2u/proprietary/bin/qseecomd:system/bin/qseecomd \
    vendor/lge/vu2u/proprietary/bin/radish:system/bin/radish \
    vendor/lge/vu2u/proprietary/bin/sensors.qcom:system/bin/sensors.qcom \
    vendor/lge/vu2u/proprietary/bin/qmuxd:system/bin/qmuxd \
    vendor/lge/vu2u/proprietary/bin/thermald:system/bin/thermald \
    vendor/lge/vu2u/proprietary/bin/cnd:system/bin/cnd \
    vendor/lge/vu2u/proprietary/bin/rmt_storage:system/bin/rmt_storage \
    vendor/lge/vu2u/proprietary/bin/rild:system/bin/rild \
    vendor/lge/vu2u/proprietary/bin/mm-pp-daemon:system/bin/mm-pp-daemon \
    vendor/lge/vu2u/proprietary/bin/mm-qcamera-daemon:system/bin/mm-qcamera-daemon \
    vendor/lge/vu2u/proprietary/bin/mpdecision:system/bin/mpdecision \
    vendor/lge/vu2u/proprietary/bin/gsiff_daemon:system/bin/gsiff_daemon \
    vendor/lge/vu2u/proprietary/bin/qseecomd:system/bin/qseecomd \
    vendor/lge/vu2u/proprietary/bin/atd:system/bin/atd \
    vendor/lge/vu2u/proprietary/bin/qcks:system/bin/qcks \
    vendor/lge/vu2u/proprietary/bin/qmiproxy:system/bin/qmiproxy \
    vendor/lge/vu2u/proprietary/bin/qosmgr:system/bin/qosmgr \
    vendor/lge/vu2u/proprietary/bin/bridgemgrd:system/bin/bridgemgrd \
    vendor/lge/vu2u/proprietary/vendor/firmware/libpn544_fw.so:system/vendor/firmware/libpn544_fw.so \
    vendor/lge/vu2u/proprietary/bin/BCM4334B0_002.001.013.0636.0687.hcd:system/bin/BCM4334B0_002.001.013.0636.0687.hcd \
    vendor/lge/vu2u/proprietary/etc/snd_soc_msm/snd_soc_msm_2x:system/etc/snd_soc_msm/snd_soc_msm_2x \
    vendor/lge/vu2u/proprietary/etc/firmware/a225_pfp.fw:system/etc/firmware/a225_pfp.fw \
    vendor/lge/vu2u/proprietary/etc/firmware/a225_pm4.fw:system/etc/firmware/a225_pm4.fw \
    vendor/lge/vu2u/proprietary/etc/firmware/a225p5_pm4.fw:system/etc/firmware/a225p5_pm4.fw \
    vendor/lge/vu2u/proprietary/etc/firmware/vidc.b00:system/etc/firmware/vidc.b00 \
    vendor/lge/vu2u/proprietary/etc/firmware/vidc.b01:system/etc/firmware/vidc.b01 \
    vendor/lge/vu2u/proprietary/etc/firmware/vidc.b02:system/etc/firmware/vidc.b02 \
    vendor/lge/vu2u/proprietary/etc/firmware/vidc.b03:system/etc/firmware/vidc.b03 \
    vendor/lge/vu2u/proprietary/etc/firmware/vidc.mdt:system/etc/firmware/vidc.mdt \
    vendor/lge/vu2u/proprietary/etc/firmware/vidcfw.elf:system/etc/firmware/vidcfw.elf \
    vendor/lge/vu2u/proprietary/etc/firmware/vidc_1080p.fw:system/etc/firmware/vidc_1080p.fw \
    vendor/lge/vu2u/proprietary/etc/firmware/fw_bcmdhd.bin:system/etc/firmware/fw_bcmdhd.bin \
    vendor/lge/vu2u/proprietary/etc/firmware/fw_bcmdhd_apsta.bin:system/etc/firmware/fw_bcmdhd_apsta.bin \
    vendor/lge/vu2u/proprietary/etc/firmware/fw_bcmdhd_mfg.bin:system/etc/firmware/fw_bcmdhd_mfg.bin \
    vendor/lge/vu2u/proprietary/etc/firmware/fw_bcmdhd_mfg.bin:system/etc/firmware/fw_bcmdhd_p2p.bin \
    vendor/lge/vu2u/proprietary/etc/init.grand-common.modem_links.sh:system/etc/init.grand-common.modem_links.sh \
    vendor/lge/vu2u/proprietary/etc/thermald-8960.conf:system/etc/thermald-8960.conf \
    vendor/lge/vu2u/proprietary/etc/vold.fstab:system/etc/vold.fstab \
    vendor/lge/vu2u/proprietary/usr/keylayout/keypad_8960.kl:system/usr/keylayout/keypad_8960.kl
